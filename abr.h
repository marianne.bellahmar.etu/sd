// abr.h


/* 
 * Un ABR est un pointeur de type struct abr *
 * L'ABR vide vaut NIL 
 * 
 */


//Déclaration de la structure d'un ABR
struct abr {struct abr* gauche;
	    float valeur;
	    struct abr* droit;
};

#include <stdbool.h>
#define NIL (struct abr *) 0

// Destructeur 
extern void clear_abr(struct abr* A);

//Ajout d'un élement dans l'ABR
extern struct abr* ajouter_abr(float valeur, struct abr* A);

// Affichage de l'ABR
extern void afficher_abr(struct abr* A);

//Hauteur de l'ABR
extern int hauteur_abr(struct abr* A);

//Nombres de noeuds présents dans l'ABR
extern int nombre_noeuds_abr(struct abr* A);

//Nouvelle feuille à ajouter à un ABR
extern struct abr* new_feuille(struct abr* A, float x);

// Dot
extern void abr_dot(struct abr* A);

// Recherche dans un ABR
extern bool rechercher_abr(float val, struct abr* A);
