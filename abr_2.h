// abr.h
// SUITE DU TP 	

/* 
 * Un ABR est un pointeur de type struct abr *
 * L'ABR vide vaut NIL 
 * 
 */
#include <wchar.h>

#define MAXLEN 80
typedef wchar_t wstring[MAXLEN];

//Déclaration de la structure d'un ABR
struct abr {struct abr* gauche;
	    struct abr* droit;
	    wstring satellite;
	    wstring cle;
};


#define NIL (struct abr *) 0

// Destructeur 
extern void clear_abr(struct abr* A);

//Ajout d'un élement dans l'ABR
extern struct abr* ajouter_abr(wstring cle, wstring satellite, struct abr* A);

// Affichage de l'ABR
extern void afficher_abr(struct abr* A);

//Hauteur de l'ABR
extern int hauteur_abr(struct abr* A);

//Nombres de noeuds présents dans l'ABR
extern int nombre_noeuds_abr(struct abr* A);

//Nouvelle feuille à ajouter à un ABR
extern struct abr* new_feuille(struct abr* A, wstring cle, wstring satellite);

// Dot
//extern void abr_dot(struct abr* A);

// Rechercher un element dans l'arbre
extern wstring* rechercher_abr(wstring val, struct abr* A);
