// main.c suite du tp



// On fait appel au fichier .h et a une bibliotheque C
#include <locale.h>
#include <wctype.h>
#include <assert.h>
#include <stdio.h>
#include "abr_2.h"

// Définition du programme principal
int main()
	{struct abr* racine; // Creation d'une variable de type struct abr *
	wstring clef;
	wstring satellite;
	wint_t c;
	FILE* f;

	assert(setlocale(LC_ALL,"C-UTF-8") !=NULL);
	f=fopen("Esperanto-Français.utf8","r");
	assert(f!=(FILE*)0);

	racine=NIL;
	c=fgetwc(f);
	while(c!=WEOF)
		{int i=0;
		while (c!=L':')
			{clef[i]=c;
			i+=1;
			c=fgetwc(f);
		}
		clef[i]=L'\0';
		c=fgetwc(f);
		i=0;
		while(c!=L'\n')
			{satellite[i]=c;
			i+=1;
			c=fgetwc(f);
		}
		satellite[i]=L'\0';
		racine=ajouter_abr(clef, satellite,racine);
		c=fgetwc(f);
	}
	fclose(f);
	//Affichage
	afficher_abr(racine);

	//Hauteur de l'arbre;
	wprintf(L"hauteur de l'arbre est %d", hauteur_abr(racine));

	// rechercher un mot
	wstring test= L"Test";
	rechercher_abr(test, racine);

	clear_abr(racine);
	
	return 0;
}


