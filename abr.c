// abr.c

// Inclusion du .h et des bibliotheques
#include "abr.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

//Definition de la fonction récursive d'ajout d'un réel x dans un ABR (racine)
struct abr* ajouter_abr(float x, struct abr* racine)
{	//Gestion du cas de l'ABR null	
	if (racine ==NIL)
		{return new_feuille(racine,x);} // On fait appel à la fonction de création d'une nouvelle feuille 
	// Lorsque l'ABR n'est pas null
	// On va tester si la valeur du reel x à ajouter est plus grande ou plus petite que la valeur indiquée dans l'arbre
	// Si elle est plus petite alors on ajoutera à gauche sinon on ajoutera x à droite 
	else if (racine-> valeur<x) 
	{	struct abr* racine_2; // On définit un nouvel ABR
		racine_2=ajouter_abr(x,racine->droit); // On re-fait appel à la même fonction pour ajouter le reel x à droite
		racine->droit = racine_2;
		return racine;
	}
	else { racine-> gauche = ajouter_abr(x,racine->gauche); // On refait appel à cette fonction récursive pour ajouter le reel à gauche
		return racine;
	}
}
//  A la fin cette fonction retournera ne nouvel ABR avec le reel x ajouté


// Création de l'action qui permettra d'afficher l'arbre
// Elle est recursive car a l'interieur de cette derniere on fait appel à cette même action
void afficher_abr(struct abr* A)
	{if (A!=NIL) // Ici on vérifie que l'ABR n'est pas vide et tant que l'on a un valeur on l'affiche
		{afficher_abr(A->gauche); 
		printf("\n %lf",A->valeur);
		afficher_abr(A->droit);
		}

	}

// Ici cette fonction retournera la hauteur de l'arbre
// Il s'agit d'un entier
int hauteur_abr(struct abr* A)
	{int haut_g, haut_d; // On definit deux variables 
	if(A!=NIL) // On effectue la suite tant que l'on a des valeurs 
		{haut_g = 1+hauteur_abr(A->gauche); // On compte, grâce à cette fonction recursive, la hauteur à gauche de la racine
		haut_d = 1+hauteur_abr(A->droit); // On fait de même pour le cote droit
		return haut_g > haut_d ? haut_g : haut_g; // Ici on compare la hauteur de chaque cote de la racine pour savoir laquelle est la plus grande qui représentera la hauteur
		}
	else return -1;// Ici c'est  le cas de l'arbre vide
	}

// Cette fonction récursive permet de calculer le nombre de noeuds présents dans l'arbre
// Elle retourne un entier
int nombre_noeuds_abr(struct abr* A)
	{if (A!=NIL)
		{return (1+nombre_noeuds_abr(A->gauche)+nombre_noeuds_abr(A->droit));} // On oublie pas d'ajouter 1 qui correspond à la racine
	else return 0; // Le cas de l'arbre composé uniquement de la racine
	}

// Permet de nettoyer l'ABR
void clear_abr(struct abr* A)
	{if (A!=NIL)
		{clear_abr(A->gauche); // recursion à gauche
		clear_abr(A->droit); // récursion à droite
		free(A); // liberation de la mémoire
		}
	}


// Fonction permettant de créer une nouvelle feuille dans l'ABR
struct abr* new_feuille(struct abr* A, float x)
	{A = malloc(sizeof(struct abr)); // Crée un espace memoire
	//Rappelons qu'une feuille est un élement qui n a pas de de successeurs ni a gauche, ni a droite
	A->gauche = NIL; //donc ici on affecte à gauche du reel x de la feuille la valeur NIL correspondant à nulle/vide
	A->valeur = x; // On affecte a la feuille sa valeur
	A->droit = NIL; // A droite on ajoute NIL
	return A; // On retourne la feuille créée (mini abr contitué d'une valeur et d'aucuns successeurs
	}

// Ici on crée la fonction abr_dot_2 
// Cette derniere est statique c'est a dire qu'elle sera utilisable uniquement dans ce fichier
//Elle permet d'affichage des valeurs de l'arbre
static void abr_dot_2(struct abr* A, FILE* f)
	{if (A->gauche!=NIL)
		{fprintf(f,"%lf -> %lf [label=\"gauche\"];\n", A->valeur, A->gauche->valeur);
		abr_dot_2(A->gauche,f);
		}
	if (A->droit!=NIL)
		{fprintf(f,"%lf -> %lf [label=\"droit\"];\n", A->valeur, A->droit->valeur);
		abr_dot_2(A->droit, f);
		}
	}


void abr_dot(struct abr* A)
	{FILE *f;
	f=fopen("ABR.dot","w"); //On ouvre le fichier dans lequel on va écrire
	assert(f !=NULL);
	fprintf(f, "digraph G{ \n"); 
	if (A==NIL)
		{fprintf(f,"Vide} \n");}
	else if (nombre_noeuds_abr(A)==1)
		{fprintf(f,"%f ; \n", A->valeur);}
	else {abr_dot_2(A,f);} // On fait appel a la fonction d'affichanche
	fprintf(f,"}\n");
	fclose(f); // On ferme le fichier dans lequel on a ecrit les commandes

	// Ici on fait appel à ces commandes linux via 'system' 
	// Ceci permet d'avoir un affichage graphiqe de l'arbre
	system("dot -Tpdf ABR.dot -Grankdir=LR -o ABR.pdf");
	system("evince ABR.pdf");
	}


bool rechercher_abr(float val, struct abr* A)
	{bool trouve=false;
	struct abr* B;
	B=A;
	while (B!=NIL && !trouve){
		if (B->valeur==val)
			{trouve=true;}
		else if (B->valeur<val)
			{B=B->droit;}
		else 
			{B=B->gauche;}
	}
	return trouve;
	}
