// main.c



// On fait appel au fichier .h et a une bibliotheque C
#include "abr.h"
#include <stdio.h>

// Définition du programme principal
int main()
	{struct abr* racine; // Creation d'une variable de type struct abr *
	float x; // Et création d'une variable x de type float
	racine = NIL; // initialisation de l'ABR
	//Demande à l'utilisateur de saisi un réel qui sera ajouté à l'arbre
	printf("saisir un reel c :");
	scanf("%f", &x);
	//Tant que ce réel est different de -un, on peut continuer a ajouter des nouvelles valeurs
	while (x!=-1)
		{racine=ajouter_abr(x, racine); // Ajout de la nouvelle valeur dans l'ABR
		afficher_abr(racine); // Affichage de l'ABR avec la nouvelle valeur
		scanf("%f", &x); // Demande à l'utiisateur la saisie d'un nouveau réel
	}
	//Permet d'afficher la hauteur de l'ABR
	printf("la hauteur de l'ABR est %d \n", hauteur_abr(racine));
	//Permet d'afficher le nombe de noeuds de l'ABR
	printf("le nombre de noeuds de l'ABR est %d \n", nombre_noeuds_abr(racine));
	//Permet l'affichage graphique de l'ABR
	abr_dot(racine);
	// Permet de rechercher une valeur dans l'ABR
	// choix de la valeur à rechercher
	float y=10;// donnée deja fixé mais peut aussi être demandé à l'utilisateur
	if(rechercher_abr(y, racine))
		{printf("la valeur 10 est dans l'arbre \n");}
	//Permet de nettoyer l'ABR
	clear_abr(racine);
	
	return 0;
}


