3 avril
-------
    Le code ne compile pas. Mets en commentaire un maximum de choses
    jusqu'à-ce que ça marche et décommente un peu à la fois.
    Commence par les premières erreurs détectées par le compilateur


Reponse :
Merci, je vais regarder ça.

14 avril
--------
    Ça compile et ça marche :-)

    Tu pourrais améliorer l'indentation du code. 
    Et même l'écriture, plus généralement, qui n'est pas très régulière.

4 mai
-----
    Bonjour Marianne,

    Un peu les mêmes commentaires pour la partie 1.

    La partie 2 compile mais ne marche pas, malheureusement.

    
